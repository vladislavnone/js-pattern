class Database {
    constructor(data) {
        if (Database.exists) {
            return Database.exists;
        }

        Database.insance = this;
        Database.exists = true;
        this.data = data;
    }

    getData() {
        return this.data;
    }
}

const mongo = new Database('MongoDB');
console.log(mongo.getData());

const mysql = new Database('mysql');
console.log(mysql.getData());