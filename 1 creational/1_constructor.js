// 1 variant

// function Server (name, ip) {
//     this.name = name;
//     this.ip = ip;
// }

// Server.prototype.getUrl = function() {
//     return `https://${this.ip}:80`;
// };

// 2 variant

class Server  {

    constructor(name, ip){
        this.name = name;
        this.ip = ip;
    }

    getUrl (){
        return `https://${this.ip}:80`;
    }
}

var aws = new Server('aws German', '80.12.32.13');

console.log(aws.getUrl());